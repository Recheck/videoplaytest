package com.almaz.videoplayertest

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_main.*

class MainViewModel: ViewModel() {
    private var currentUri : Uri? = null
    val playerState = MutableLiveData<Pair<Boolean, Int>>()

    fun playPressed(isPlaying: Boolean, textUrl : String) {
        val newUri = Uri.parse(textUrl)
        if (currentUri == newUri) {
            if (isPlaying) {
                playerState.value = Pair(false, R.string.play)
            } else {
                playerState.value = Pair(true, R.string.pause)
            }
        }
        currentUri = newUri
    }
}