package com.almaz.videoplayertest

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.offline.DownloadHelper
import com.google.android.exoplayer2.offline.DownloadService
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException


class MainActivity : AppCompatActivity() {
    lateinit var player: SimpleExoPlayer
    lateinit var uri: Uri
    lateinit var downloadHelper: DownloadHelper
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        val dataSourceFactory: DataSource.Factory = (application as App).buildDataSourceFactory()

        player = SimpleExoPlayer.Builder(this).build()
        playerView.player = player
        playerView.useController = false

        viewModel.playerState.observe(this, Observer {
            player.playWhenReady = it.first
            btnPlay.text = getString(it.second)
        })

        btnPlay.setOnClickListener {
            viewModel.playPressed(player.isPlaying, editUrl.text.toString())
            val videoSource: MediaSource = ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(editUrl.text.toString()))
            player.prepare(videoSource)
        }

        btnDownload.setOnClickListener {
            startDownloading()
        }
    }

    private fun startDownloading() {
        val type = Util.inferContentType(uri, null)
        if (type == C.TYPE_OTHER) {
            downloadHelper = DownloadHelper.forProgressive(this, uri)
            downloadHelper.prepare(object : DownloadHelper.Callback {
                override fun onPrepared(helper: DownloadHelper) {
                    if (helper.periodCount == 0) {
                        val downloadRequest = helper.getDownloadRequest(Util.getUtf8Bytes("video1"))
                        DownloadService.sendAddDownload(
                            this@MainActivity,
                            VideoDownloadService::class.java,
                            downloadRequest,
                            false
                        )
                        helper.release()
                    }
                    hideError()
                }

                override fun onPrepareError(helper: DownloadHelper, e: IOException) {
                    showError(R.string.download_error)
                }

            })
        } else {
            showError(R.string.invalid_video)
        }
    }

    private fun showError(textId : Int) {
        textInvalidVideo.text = getString(textId)
        textInvalidVideo.visibility = View.VISIBLE
    }

    private fun hideError() {
        textInvalidVideo.visibility = View.GONE
    }

    override fun onDestroy() {
        super.onDestroy()
        player.release()
        downloadHelper.release()
    }
}
