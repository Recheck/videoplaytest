package com.almaz.videoplayertest

import android.app.Notification
import android.util.Log
import com.almaz.videoplayertest.App.DOWNLOAD_NOTIFICATION_CHANNEL_ID
import com.google.android.exoplayer2.database.DatabaseProvider
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.offline.Download
import com.google.android.exoplayer2.offline.DownloadManager
import com.google.android.exoplayer2.offline.DownloadService
import com.google.android.exoplayer2.scheduler.PlatformScheduler
import com.google.android.exoplayer2.scheduler.Scheduler
import com.google.android.exoplayer2.ui.DownloadNotificationHelper
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.Cache
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import com.google.android.exoplayer2.util.NotificationUtil
import com.google.android.exoplayer2.util.Util
import java.io.File


class VideoDownloadService :
    DownloadService(1, 1000, DOWNLOAD_NOTIFICATION_CHANNEL_ID, R.string.exo_download_notification_channel_name, 0) {
    private lateinit var databaseProvider: DatabaseProvider
    private var downloadDirectory: File? = null
    private lateinit var downloadCache: Cache
    private var downloadManager: DownloadManager? = null
    private val downloadNotificationHelper: DownloadNotificationHelper? = null
    private lateinit var dataSourceFactory : DefaultHttpDataSourceFactory
    protected var userAgent: String? = null

    override fun getDownloadManager(): DownloadManager {
        val downloadManager = (application as App).downloadManager
        val downloadNotificationHelper: DownloadNotificationHelper =
            (application as App).downloadNotificationHelper
        downloadManager.addListener(object : DownloadManager.Listener {
            private var nextNotificationId = 2
            override fun onDownloadChanged(downloadManager: DownloadManager, download: Download) {
                Log.d("myLogs", "onDownloadChanged ${download.state}")
                var notification: Notification
                if (download.state == Download.STATE_COMPLETED) {
                    notification = downloadNotificationHelper.buildDownloadCompletedNotification(
                        R.mipmap.ic_launcher,  /* contentIntent= */
                        null,
                        Util.fromUtf8Bytes(download.request.data)
                    )
                } else if (download.state == Download.STATE_FAILED) {
                    notification = downloadNotificationHelper.buildDownloadFailedNotification(
                        R.mipmap.ic_launcher,  /* contentIntent= */
                        null,
                        Util.fromUtf8Bytes(download.request.data)
                    )
                } else { return
//                    notification = downloadNotificationHelper.buildProgressNotification(
//                        R.mipmap.ic_launcher,  /* contentIntent= */
//                        null,
//                        "Downloading",
//                        arrayListOf()
//                    )
                }
                NotificationUtil.setNotification(this@VideoDownloadService, nextNotificationId++, notification)
            }
        }
        )
        Log.d("myLogs", "DownloadManager = ${downloadManager.currentDownloads} ${downloadManager.downloadIndex}")
        return downloadManager
    }

    override fun getForegroundNotification(downloads: MutableList<Download>): Notification {
        Log.d("myLogs", "getForegroundNotification $downloads")
        for (d in downloads) {
            Log.d("myLogs", "D = ${d.state} ${d.request.uri}")
        }
        Log.d(
            "myLogs",
            "DownloadManagerForeground = ${downloadManager?.currentDownloads} ${downloadManager?.downloadIndex}"
        )
        return (application as App)
            .downloadNotificationHelper
            .buildProgressNotification(
                R.mipmap.ic_launcher,  /* contentIntent= */null,  /* message= */null, downloads
            )
    }

    override fun getScheduler(): Scheduler? {
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("myLogs", "onDestroyCalled")
    }
}